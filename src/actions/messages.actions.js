import * as actions from './actions'
import * as socketApi from '../api/socket'

const newMessage = message => {
  const date = new Date().getTime()
  message['id'] = date
}

export const addBotMessage = message => {
  message['id'] = new Date()
  message['bot'] = true
  return {
    type: actions.MESSAGE_BOT,
    payload: message
  }
}

export const messageReceived = message => {
  newMessage(message)
  return {
    type: actions.MESSAGE_RECEIVED,
    payload: message
  }
}

export const sendMessage = (socket, message) => {
  socketApi.sendMessage(socket, message)
  newMessage(message)
  message['me'] = true
  return {
    type: actions.MESSAGE_SEND_REQ,
    payload: message
  }
}

export const resetMessages = () => {
  return {
    type: actions.MESSAGE_RESET
  }
}
