export const MESSAGE_BOT = 'message/MESSAGE_BOT'
export const MESSAGE_RECEIVED = 'message/MESSAGE_RECEIVED'
export const MESSAGE_RESET = 'message/MESSAGE_RESET'
export const MESSAGE_SEND_REQ = 'message/MESSAGE_SEND_REQ'

export const RESET_SETTINGS = 'settings/RESET_SETTINGS'
export const RETREIVE_SETTINGS = 'settings/RETREIVE_SETTINGS'
export const SET_CONFIG = 'settings/SET_CONFIG'
export const SET_STRINGS = 'settings/SET_STRINGS'
