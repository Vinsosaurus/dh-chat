import * as actions from './actions'

export const resetSettings = () => {
  return {
    type: actions.RESET_SETTINGS
  }
}
export const setConfig = (name, value) => {
  return {
    type: actions.SET_CONFIG,
    payload: {name: name, value: value}
  }
}
export const retreiveConfig = settings => {
  return {
    type: actions.RETREIVE_SETTINGS,
    payload: settings
  }
}

export const setStrings = (strings) => {
  return {
    type: actions.SET_STRINGS,
    payload: strings
  }
}
