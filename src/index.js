import 'core-js/fn/object/assign'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import { SocketProvider } from 'socket.io-react'

//
import { connectSocket } from './api/socket'
import store, { history } from './store'
import App from './components/Main'

const socket = connectSocket()
const target = document.getElementById('app')

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <SocketProvider socket={socket}>
        <App />
      </SocketProvider>
    </ConnectedRouter>
  </Provider>
  , target
);
