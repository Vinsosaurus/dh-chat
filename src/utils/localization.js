import LocalizationStrings from 'react-localization'

import en from '../static/langs/en'
import fr from '../static/langs/fr'

const strings = new LocalizationStrings({en, fr})

export const languages = [
  {
    label: 'English',
    value: 'en'
  }, {
    label: 'Français',
    value: 'fr'
  }
]

export function setLanguage (language) {
  strings.setLanguage(language)
  return strings
}
