export function addZero(i) {
  if (i < 10) {
      i = '0' + i;
  }
  return i;
}

export function getTime(date, clockdisplay) {
  const twentyfour = clockdisplay === '24'
  const h = addZero(date.getHours())
  const m = addZero(date.getMinutes())
  const pm = h >= 12

  return (twentyfour ? h : pm ?  h - 12 : h) + ':' + m + (twentyfour ? '' : h < 13 ? ' am' : ' pm')
}
