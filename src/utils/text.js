export const emojis = {
  'o\/': '👋',
  ':\\)': '😊',
  ':D': '😀',
  ';\\)': '😉',
  ':\\(': '🙁',
  ':\'\\(': '😪',
  ':\\/': '😕',
  'oO': '🤔',
  '<3': '❤️',
  ':robot:': '🤖'
}

export function parseText(text) {
  return aliveEmojis(text)
}

export function aliveEmojis(text) {
  Object.keys(emojis).map( emoji => {
    const regex = RegExp(emoji, 'g')
    text = text.toString().replace(regex, emojis[emoji])
  })
  return text
}

export function getBlocks(text) {
  return text.split('\u000A')
}

export function breakline(text) {
  text = text.toString().replace('\u000A', '\n')
  return text
}
