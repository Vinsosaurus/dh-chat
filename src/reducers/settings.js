
import * as actions from '../actions/actions'
import { setLanguage } from '../utils/localization'

const initialState = {
  settings: {
    clockdisplay: '12',
    darkmode: false,
    language: 'en',
    nickname: null,
    sendShortCut: true
  },
  strings: setLanguage('en')
}

export default (state = initialState, action) => {
  switch(action.type) {
    case actions.RESET_SETTINGS:
      return {
        ...state,
        settings: initialState.settings
      }
    case actions.RETREIVE_SETTINGS:
      return {
        ...state,
        settings: action.payload
      }
    case actions.SET_CONFIG:
      const settings = {...state.settings, [action.payload.name]: action.payload.value}
      return {
        ...state,
        settings: settings
      }
    case actions.SET_STRINGS:
      return {
        ...state,
        strings: action.payload
      }
    default: return state
  }
}
