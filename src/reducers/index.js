import 'babel-polyfill';
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

//
import messages from './messages';
import settings from './settings';

export default combineReducers({
  routing: routerReducer,
  //
  messages: messages,
  settings: settings
})
