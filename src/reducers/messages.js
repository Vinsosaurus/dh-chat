import * as actions from '../actions/actions'

const initialState = {
  messages: [
    {
      id: '0',
      user: 'welcomeBot2000',
      message: 'Welcome on ChatRoom 2000!'
    }
  ]
}

export default (state = initialState, action) => {
  switch(action.type) {
    case actions.MESSAGE_RECEIVED:
      return {
        ...state,
        messages: [...state.messages, action.payload]
      }
    case actions.MESSAGE_RECEIVED:
      return {
        ...state,
        messages: [...state.messages, action.payload]
      }
    case actions.MESSAGE_RESET:
      return {
        ...state,
        messages: []
      }
    case actions.MESSAGE_SEND_REQ:
      return {
        ...state,
        messages: [...state.messages, action.payload]
      }
    default: return state
  }
}
