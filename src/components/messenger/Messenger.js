require('./messenger.scss')

import React from 'react'
import MessageEditor from './message-editor/MessageEditor';

class Messenger extends React.Component {
  state = {
    message: ''
  }

  handleMessageChanged = message => {
    this.setState({ message: message })
  }

  render() {
    return (
      <div className="messenger padding-1">
        <MessageEditor {...this.props} changed={this.handleMessageChanged} />
      </div>
    )
  }
}

export default Messenger
