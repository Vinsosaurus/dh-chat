import React from 'react'
import PropTypes from 'prop-types'

import {Editor, EditorState, ContentState} from 'draft-js'
import { aliveEmojis } from '../../../utils/text'

class MessageEditor extends React.Component {
  static propTypes = {
    settings: PropTypes.object.isRequired,
    socket: PropTypes.object.isRequired,
    changed: PropTypes.func
  }

  state = {
    editorState: EditorState.createEmpty()
  }

  handleClick = e => {
    e.preventDefault()
    this.editor.focus()
  }

  handleChange = editorState => {
    this.formatText( editorState, (message, updated) => {
      const _editorState = updated ? this.createEditorState(message) : editorState
      this.setState({ editorState: _editorState }, () => {
        if (this.props.changed) this.props.changed(message)
      })
    })
  }

  handleReturn = e => {
    if (this.props.settings.sendShortCut && e.ctrlKey) {
      this.sendMessage()
    }
  }

  handleClickSend = () => {
    this.sendMessage()
  }

  /**
   * Format the given text and pass it as callback argument
   * the callback also receive a boolean, telling that message has mutate
   * @param {object} editorState
   * @param {func} callback
   */
  formatText (editorState, callback) {
    const message = editorState.getCurrentContent().getPlainText()
    const _message = aliveEmojis(message)
    callback(_message, message !== _message)
  }

  /**
   * Create a new editorState object based on the given string
   * @param {string} message
   */
  createEditorState (message) {
    const contentState = ContentState.createFromText(message)
    const editorState = EditorState.createWithContent(contentState)
    return editorState
  }

  sendMessage = () => {
    let message = this.state.editorState
                        .getCurrentContent()
                        .getPlainText()
                        .replace(/^\s*(\\u003)*\s*/gm, '') // remove lines of only breakline or whitespace
    if (message.replace(/ /g, '').replace(/\u000A/g, '').length > 0) {
      const _editorState = EditorState.createEmpty()
      this.setState({editorState: _editorState}, () => {
        this.props.sendMessage(this.props.socket, {user: 'me', message: message})
      })
    }
  }

  render () {
    return (
      <div className="message-editor">
        <div className="editor-wrapper" onClick={this.handleClick}>
          <Editor
            editorState={this.state.editorState}
            onChange={this.handleChange}
            handleReturn={this.handleReturn}
            ref={(ref) => this.editor = ref}
            focus />
        </div>
        <button onClick={this.handleClickSend}>Send</button>
      </div>
    );
  }
}

export default MessageEditor
