import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { socketConnect } from 'socket.io-react'

import { sendMessage } from '../../actions/messages.actions'
import Messenger from './Messenger'

const mapDispatchToProps =  dispatch => bindActionCreators({
  sendMessage: (socket, message) => sendMessage(socket, message)
}, dispatch)

export default socketConnect(connect(null, mapDispatchToProps)(Messenger))
