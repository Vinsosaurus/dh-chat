import React from 'react'
import PropTypes from 'prop-types'

class DarkMode extends React.PureComponent {
  static propTypes = {
    settings: PropTypes.object.isRequired,
    setConfig: PropTypes.func.isRequired
  }

  state = {
    darkmode: this.props.settings.darkmode
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.settings.darkmode !== this.state.darkmode) { this.setState({darkmode: nextProps.settings.darkmode})}
  }

  handleClick = () => {
    const darkmode = this.state.darkmode ? false : true
    this.props.setConfig('darkmode', darkmode)
  }

  getDarkModeButtonLabel() {
    return `${this.props.strings.darkmode}: ` + (this.state.darkmode ? this.props.strings.on : this.props.strings.off)
  }

  render() {
    return(
      <button onClick={this.handleClick}>
        {this.getDarkModeButtonLabel()}
      </button>
    )
  }
}

export default DarkMode;
