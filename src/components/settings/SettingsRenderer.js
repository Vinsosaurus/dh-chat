/**
 * This render props component ensures its rendered elements to receive the store states they need
 */
import React from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { setConfig, resetSettings, setStrings } from '../../actions/settings.actions'

class SettingsRenderer extends React.PureComponent {
  static propTypes = {
    render: PropTypes.func.isRequired
  }

  /**
   * Pass an object made of store states and actions to the render props function,
   * and give power to your modules!!
   */
  renderProps () {
    const connect = {
      settings: this.props.settings,
      strings: this.props.strings,
      setConfig: this.props.setConfig
    }
    return this.props.render(connect)
  }

  render () {
    return (
      <div>
        { this.renderProps()}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    settings: state.settings.settings,
    strings: state.settings.strings
  }
}

const mapDispatchToProps =  dispatch => bindActionCreators({
  resetSettings: (name, value) => resetSettings(name, value),
  setConfig: (name, value) => setConfig(name, value),
  setStrings: (strings) => setStrings(strings)
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(SettingsRenderer)
