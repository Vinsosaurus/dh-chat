import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { resetSettings, setConfig, setStrings } from '../../actions/settings.actions'
import Settings from './Settings'

const mapStateToProps = state => {
  return {
    settings: state.settings.settings,
    strings: state.settings.strings
  }
}

const mapDispatchToProps =  dispatch => bindActionCreators({
  resetSettings: (name, value) => resetSettings(name, value),
  setConfig: (name, value) => setConfig(name, value),
  setStrings: (strings) => setStrings(strings)
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
