require('./settings.scss')

import React from 'react'

import { languages, setLanguage } from '../../utils/localization'
import { Fieldset, Check, Radio, Select } from '../shared';
import { SettingsFooter } from './SettingsFooter';
import Profile from './Profile';

export const Settings = props => {
  const handleNicknameChange = value => {
    props.setConfig('nickname', value)
  }

  const handleLanguageChange = value => {
    props.setConfig('language', value)
    props.setStrings(setLanguage(value))
  }

  const handleDarkmodeChange = value => {
    props.setConfig('darkmode', value)
  }

  const handleClockDisplayChange = value => {
    props.setConfig('clockdisplay', value)
  }

  const handleSendShortcutChange = value => {
    props.setConfig('sendShortCut', value)
  }

  const { strings, settings } = props;
  return (
    <div className="padding-1">
      <Fieldset label={strings.profile}>
        <Profile nicknameChanged={handleNicknameChange} />
      </Fieldset>
      <Fieldset label={strings.general}>
        <Select
          value={settings.language}
          options={languages}
          changed={handleLanguageChange}
          label={strings.language}
        />
        <Check
          name="darkmode"
          labelOn={strings.darkmode + ' ' + strings.on + ' 😱'}
          labelOff={strings.darkmode + ' ' + strings.off}
          value={settings.darkmode}
          changed={handleDarkmodeChange} />
      </Fieldset>
      <Fieldset label={strings.chatroom}>
        <Radio
          name="clockdisplay"
          value={settings.clockdisplay}
          options={[
            {
              label: `12 ${strings.hours}`,
              value: '12'
            }, {
              label: `24 ${strings.hours}`,
              value: '24'
            }
          ]}
          changed={handleClockDisplayChange} />
          <Check
          name="sendshortcut"
          label={strings.send_shortcut}
          value={settings.sendShortCut}
          changed={handleSendShortcutChange} />
      </Fieldset>

      <SettingsFooter {...props} />
    </div>
  )
}

export default Settings;
