import React from 'react'
import Text from '../shared/Text';
import Button from '../shared/Button';

class Profile extends React.PureComponent {
  state = {
    nickname: 'Guest2000'
  }

  handleClickSaveNickname = () => {
    if (this.isNicknameValide()) {
      if (this.props.nicknameChanged) this.props.nicknameChanged(this.state.nickname)
    }
  }

  handleNicknameChange = value => {
    this.setState({nickname: value})
  }

  isNicknameValide () {
    return this.state.nickname.replace(/ /g, '').length > 0
  }

  render () {
    return (
      <div>
        <Text value={this.state.nickname} {...this.props} changed={this.handleNicknameChange} required />
        <Button clicked={this.handleClickSaveNickname} disabled={!this.isNicknameValide}>Save</Button>
      </div>
    )
  }
}

export default Profile
