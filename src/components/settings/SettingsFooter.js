import React from 'react';

import Button from '../shared/Button';

export const SettingsFooter = ({ resetSettings }) => {
  const handleResetClick = () => {
    resetSettings()
  }

  return (
    <div className="settings-footer padding-1 padding-b-2">
      <Button clicked={handleResetClick}>RESET</Button>
    </div>
  )
}
