import React from 'react'
import socketConnect from 'socket.io-react/build/socket-connect';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { onMessageReceived } from '../api/socket'
import { messageReceived } from '../actions/messages.actions'

class Chat extends React.Component {
  constructor(props) {
    super(props)
    this.initMessages()
  }

  initMessages() {
    onMessageReceived(this.props.socket, message => {
      if (message.user !== 'echoBot2000') {
        this.props.messageReceived(message)
      }
    })
  }
  render() {
    const { children } = this.props
    return <div>{children}</div>
  }
}
const mapDispatchToProps = dispatch => bindActionCreators({
  messageReceived: (message) => messageReceived(message)
}, dispatch)

export default socketConnect(Chat)
