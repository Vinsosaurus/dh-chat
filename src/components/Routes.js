import React from 'react'
import { Route } from 'react-router-dom'

import Chatroom from './chatroom/ChatroomContainer'
import Settings from './settings/SettingsContainer'

export const Routes = () => (
  <div>
    <Route exact path="/" component={Chatroom} />
    <Route exact path="/settings" component={Settings} />
  </div>
)
