require('./bubble.scss')

import React from 'react';
import PropTypes from 'prop-types'
import classNames from 'classnames'

const Bubble = ({ message, strings }) => {
  const classnames = classNames(
    {
      bubble: true,
      me: message.me,
      them: !message.me
    }
  )

  return (
    <div>
      <span className={classnames}>
        { message.user &&
          <small>{message.me ? strings.you_said : `${message.user} ${strings.said}`}</small>
        }
        <p>{message.message}</p>
      </span>
    </div>
  )
}

Bubble.propTypes = {
  message: PropTypes.object.isRequired,
  strings: PropTypes.object.isRequired
}

export default Bubble
