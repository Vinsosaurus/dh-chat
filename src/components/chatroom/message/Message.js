import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import Bubble from '../bubble/Bubble'
import { getTime } from '../../../utils/date'
import { aliveEmojis } from '../../../utils/text'

class Message extends React.Component {
  static propTypes = {
    message: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired
  }

  state = {
    message: this.props.message
  }

  componentWillMount() {
    this.setupMessage()
  }

  /**
   * Setup the message with the current settings
   */
  setupMessage() {
    const { message, settings } = this.props
    const date = new Date(message.id)
    message['date'] = message.id == 0 ? '' : getTime(date, settings.clockdisplay)
    message['message'] = aliveEmojis(message['message'])
    this.setState({
      message: message
    })
  }

  render () {
    const { message } = this.props
    var classnames = classNames({
      message: true,
      f: true,
      'f-in': message.me
    });
    return (
      <div> { /* that `div` avoids some squeezing effect on the chatbox */ }
        <div className={classnames}>
          <Bubble  {...this.props} message={message} />
          { message.date &&
            <small className="dm-text padding-l-05 padding-r-05">&nbsp;{message.date}&nbsp;</small>
          }
        </div>
      </div>
    )
  }
}

export default Message
