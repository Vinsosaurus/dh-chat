require('./chatroom.scss');

import React from 'react'
import PropTypes from 'prop-types'

import MessengerContainer from '../messenger/MessengerContainer'
import Message from './message/Message';

class Chatroom extends React.Component {
  static propTypes = {
    messages: PropTypes.arrayOf(PropTypes.object).isRequired
  }

  state = {
    messages: this.props.messages
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.messages !== this.state.messages) this.setState({messages : nextProps.messages})
  }

  renderMessage() {
    return this.state.messages.map( (message, key) => {
      return (
        <Message key={key} message={message} {...this.props}/>
      )
    })
  }

  render() {
    return (
      <div className="chatroom padding-1 padding-l-2">
        <div className="messages-box">
          { this.renderMessage() }
        </div>
        <MessengerContainer {...this.props} />
      </div>
    )
  }
}

export default Chatroom
