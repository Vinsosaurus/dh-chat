import { connect } from 'react-redux'

import Chatroom from './Chatroom'

const mapStateToProps = state => {
  return {
    messages: state.messages.messages,
    settings: state.settings.settings,
    strings: state.settings.strings
  }
}

export default connect(mapStateToProps)(Chatroom)
