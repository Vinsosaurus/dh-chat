require('normalize.css/normalize.css')
require('../scss/main.scss')

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux'
import { socketConnect } from 'socket.io-react'

import { onMessageReceived } from '../api/socket'
import { messageReceived } from '../actions/messages.actions'
import { setConfig, retreiveConfig, setStrings } from '../actions/settings.actions'
import { Routes } from './Routes'
import { Toolbar } from './toolbar/Toolbar';
import { setLanguage } from '../utils/localization';

class App extends React.Component {
  static propTypes = {
    socket: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.initSettings()
    this.onMessageReceived(this.props.socket)
  }

  componentWillReceiveProps(nextProps) {
    // save settings each time they change
    if (nextProps.settings) {
      this.saveSettings(nextProps.settings)
    }
  }

  /**
   * Check if settings are already saved and use them,
   * or then save the default ones
   */
  initSettings() {
    const settings = localStorage.getItem('settings')
    if (settings) {
      const _settings = JSON.parse(settings)
      this.props.retreiveConfig(_settings)
      this.props.setStrings(setLanguage(_settings.language))
    } else {
      this.saveSettings(this.props.settings)
    }
  }

  /**
   * Save stringify settings to local storage
   * @param {object} settings
   */
  saveSettings(settings) {
    localStorage.setItem('settings', JSON.stringify(settings))
  }

  /**
   * Send messageReceived action each time a message is receive from socket
   * @param {object} socket
   */
  onMessageReceived(socket) {
    onMessageReceived(socket, message => {
      if (message.user !== 'echoBot2000') {
        this.props.messageReceived(message)
      }
    })
  }

  render() {
    return (
      <div className={this.props.settings.darkmode ? 'app dm' : 'app'}>
        <Toolbar {...this.props}/>
        <main>
          <Routes />
        </main>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    settings: state.settings.settings,
    strings: state.settings.strings
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  messageReceived: message => messageReceived(message),
  retreiveConfig: settings => retreiveConfig(settings),
  setConfig: (name, value) => setConfig(name, value),
  setStrings: (strings) => setStrings(strings)
}, dispatch)

export default socketConnect(withRouter(connect(mapStateToProps, mapDispatchToProps)(App)));
