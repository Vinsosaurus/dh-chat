require('./menu.scss')

import React from 'react';
import MenuItem from './menu-item/MenuItem';

export const Menu = props => (
    <div className="menu">
      <MenuItem to='/' countMessages {...props} label={props.strings.chatroom} />
      <MenuItem to='/settings' {...props} label={props.strings.settings} />
    </div>
)
