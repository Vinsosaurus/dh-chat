import React from 'react';
import { connect } from 'react-redux'

import { Menu } from './Menu';

const mapStateToProps = state => {
  return {
    strings: state.settings.strings,
    settings: state.settings.settings,
    messages: state.messages.messages,
    route: state.routing.location.pathname
  }
}

export default connect(mapStateToProps)(Menu)
