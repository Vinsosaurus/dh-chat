require('./menuItem.scss')

import React from 'react'
import { Link } from 'react-router-dom'

class MenuItem extends React.Component {
  state = {
    counter: 0,
    messages: this.props.messages,
    route: this.props.route
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.countMessages && this.props.route !== this.props.to) {
      if(nextProps.route !== this.state.route) {
        this.setState({route: nextProps.route, counter: this.state.messages.length, messages: this.state.messages})
      }
      if(nextProps.messages !== this.state.messages) {
        this.setState({messages: nextProps.messages})
      }

    }
  }

  calc () {
    return this.state.messages.length - this.state.counter
  }

  render () {
    const { to, route, countMessages } = this.props
    return (
      <span className="menu-item">
        <Link to={to}>{this.props.label}</Link>
        { countMessages && this.calc() > 0 && route !== to &&
          <span>{this.calc()}</span>
        }
      </span>
    )
  }
}

export default MenuItem
