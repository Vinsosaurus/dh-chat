require('./toolbar.scss')

import React from 'react'

import DarkModeButton from '../settings/DarkMode';
import SettingsRenderer from '../settings/SettingsRenderer';
import MenuContainer from './menu/MenuContainer';

export const Toolbar = () => (
  <header className="toolbar f f-sb f-vc">
    <MenuContainer />
    <SettingsRenderer render={ (props) => (
      <DarkModeButton {...props} />
    )} />
  </header>
)
