import React from 'react'
import PropTypes from 'prop-types'

class Radio extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    label: PropTypes.string,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    ),
    changed: PropTypes.func
  }

  state = {
    value: this.props.value
  }

  componentWillReceiveProps (nextProps) {
    const value = nextProps.value;
    if (value !== this.state.value) this.setState({ value: value })
  }

  handleChange = e => {
    if (this.state.value !== e.target.value) {
      this.setState({ value: e.target.value}, () => {
        if (this.props.changed) this.props.changed(this.state.value)
      })
    }
  }

  renderRadios () {
    return this.props.options.map( (option, key) => {
      const id = this.props.name + '_' + key
      return (
        <span key={key}>
          <input type="radio"
            id={id}
            className="form-check-input"
            value={option.value}
            checked={option.value === this.state.value}
            onChange={this.handleChange} />
          <label htmlFor={id} className="form-check-label margin-r-2">
            {option.label}
          </label>
        </span>
      )
    })
  }

  render () {
    return (
      <div className="form-check">
        { this.renderRadios() }
      </div>
    )
  }
}

export default Radio
