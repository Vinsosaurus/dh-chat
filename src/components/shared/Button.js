import React from 'react'

class Button extends React.Component {
  handleClick = () => {
    if (this.props.clicked) this.props.clicked()
  }

  render () {
    return (
      <button onClick={this.handleClick}>{this.props.children}</button>
    )
  }
}

export default Button
