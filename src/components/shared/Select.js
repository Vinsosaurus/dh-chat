import React from 'react'
import PropTypes from 'prop-types'

class Select extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    ),
    changed: PropTypes.func
  }

  state = {
    value: this.props.value
  }

  componentWillReceiveProps (nextProps) {
    const value = nextProps.value;
    if (value !== this.state.value) this.setState({ value: value })
  }

  handleChange = e => {
    this.setState({ value: e.target.value}, () => {
      if (this.props.changed) this.props.changed(this.state.value)
    })
  }

  renderOptions () {
    return this.props.options.map( (option, key) => {
      return (
        <option key={key} value={option.value}>
          {option.label}
        </option>
      )
    })
  }

  render () {
    return (
      <div className="form-group">
        <label>
          {this.props.label}
        </label>
        &nbsp;
        <select name={this.props.label} onChange={this.handleChange} value={this.state.value}>
          { this.renderOptions() }
        </select>

      </div>
    )
  }
}

export default Select
