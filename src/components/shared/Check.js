import React from 'react'
import PropTypes from 'prop-types'

class Check extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.bool.isRequired,
    label: PropTypes.string,
    labelOn: PropTypes.string,
    labelOff: PropTypes.string,
    changed: PropTypes.func
  }

  state = {
    value: this.props.value
  }

  componentWillReceiveProps (nextProps) {
    const value = nextProps.value;
    if (value !== this.state.value) this.setState({ value: value })
  }

  handleChange = () => {
    this.setState({ value: !this.state.value}, () => {
      if (this.props.changed) this.props.changed(this.state.value)
    })
  }

  render () {
    return (
      <div className="checkbox form-check">
        <input type="checkbox"
        className="form-check-input"
        id={this.props.name}
        checked={this.state.value}
        onChange={this.handleChange} />

        <label htmlFor={this.props.name} className="form-check-label">
          {
            this.props.labelOn && this.props.labelOff ?
              this.state.value ? this.props.labelOn : this.props.labelOff
            : this.props.label
          }
        </label>
      </div>
    )
  }
}

export default Check
