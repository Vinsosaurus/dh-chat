import React from 'react'

export const Fieldset = props => (
  <div className="margin-b-2">
    <h2 className="margin-b-1">{props.label}</h2>
    { props.children }
  </div>
)
