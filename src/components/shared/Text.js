import React from 'react'

class Text extends React.Component {
  state = {
    value: this.props.value
  }

  handleChange = e => {
    const value = e.target.value
    this.setState({ value: value }, () => {
      if (this.props.changed) this.props.changed(value)
    })

  }

  render() {
    const { value } = this.state
    return (
      <span>
        <input value={value} onChange={this.handleChange} required />
        <label>
          { this.props.label }
        </label>
      </span>
    )
  }
}

export default Text
