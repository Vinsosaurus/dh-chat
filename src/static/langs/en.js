const en = {
  'chatroom': 'Chatroom',
  'clockdisplay': 'Clock display',
  'darkmode': 'Dark mode',
  'general': 'General',
  'typeText': 'Type some text',
  'from': 'from',
  'hours': 'hours',
  'language': 'Language',
  'on': 'Enabled',
  'off': 'Disabled',
  'options': 'Options',
  'profile': 'Profile',
  'settings': 'Settings',
  'send_shortcut': 'Send message on CTRL + ENTER',
  'type_your_message_here': 'Type your message here and send it!',
  'you_said': 'You said',
  'said': 'said'
}

export default en
