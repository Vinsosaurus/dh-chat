const fr = {
  'chatroom': 'Salon',
  'clockdisplay': 'Système horaire',
  'darkmode': 'Mode sombre',
  'typeText': 'Taper votre texte',
  'from': 'de',
  'general': 'Général',
  'hours': 'heures',
  'language': 'Langue',
  'on': 'Activé',
  'off': 'Désactivé',
  'options': 'Options',
  'profile': 'Profile',
  'settings': 'Configuration',
  'send_shortcut': 'Utiliser CTRL + ENTER pour envoyer le message',
  'type_your_message_here': 'Entrez un message et envoyez-le!',
  'you_said': 'Vous avez dit',
  'said': 'a dit'
}

export default fr
