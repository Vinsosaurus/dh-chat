import io from 'socket.io-client';

export function connectSocket () {
  return io.connect('http://185.13.90.140:8081');
}

export function onMessageReceived (socket, callback) {
  socket.on('message', message => {
    callback(message)
  })
}

export function sendMessage (socket, message) {
  socket.emit('message', message)
}
