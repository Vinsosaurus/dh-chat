__CHATROOM 2000__
=======================

Hi 👋 Welcome on Chat Room 2000


# USAGE
Make sure that yarn is intalled. If not, you can install it using [brew](https://brew.sh/)

`brew install yarn`

To chat with a bot of poor conversation from 2000

`yarn && yarn start`

# HOW TO USE
We propose you, in a way to make your day memorable, to infinitly receive a sympathic welcome message from [a real robot](https://i.pinimg.com/564x/35/87/fd/3587fdf9433d8fa32008c89e3783b7df.jpg) coming from the beginning of the millenium. Also, you'll be able to chat with him! ...but in all honesty though, don't expect to receive any answer.

## Type some text
We use _Draft.js_ to manage the textarea. It offers us easy setup and lot of possibilities for v next.

You can use some emojis
👋
😊
😀
😉
😪
🤔
❤️
🤖

## Store
State management is handled by _Redux_ and we use _localStorage_ to store localy the app settings.

# MISCELLANEOUS

## Supported browsers
- Chrome 67
- Safari 11
- IE 11
- Firefox 60

## Known issues
- No more scroll over the chatroom! oO
- CTRL + ENTER shortcut does't clear the text, and doesn't work on IE 11 and Firefox

## What's next?
- Write the tests!
- Add a collapsable emojis board, from where user can click and write emoji
- Call a web designer
